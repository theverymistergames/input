﻿namespace MisterGames.Input.Bindings {

    public abstract class InputBindingAxisBase : InputBinding {

        public abstract float GetValue();

    }

}